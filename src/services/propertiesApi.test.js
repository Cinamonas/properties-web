jest.mock('axios', () => ({
  get: jest.fn(),
  post: jest.fn(),
  put: jest.fn(),
}));

jest.mock('../config', () => ({
  apiUrl: 'api://host.com',
}));

import axios from 'axios';

import { apiUrl } from '../config';
import * as api from './propertiesApi';

describe('propertiesApi', () => {

  describe('fetchAll()', () => {
    let data;

    beforeEach(() => {
      data = [
        { id: 123 },
        { id: 456 },
      ];
      axios.get.mockReturnValueOnce(Promise.resolve({ data }));
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('should fetch from correct endpoint', async () => {
      await api.fetchAll();
      expect(axios.get.mock.calls[0][0]).toBe(`${apiUrl}/properties`);
    });

    it('should return properties', async () => {
      expect(await api.fetchAll()).toBe(data);
    });

  });

});
