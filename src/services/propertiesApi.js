import axios from 'axios';
import config from '../config';

const API_URL = config.apiUrl;

export function fetchAll() {
  return axios.get(`${API_URL}/properties`).then(({ data }) => data);
}

export function fetch(id) {
  return Promise.resolve({ id }); // noop
}
