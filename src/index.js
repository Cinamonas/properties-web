import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import './utils/shims';
import { App } from './components';
import * as stores from './stores';

ReactDOM.render((
  <Provider {...stores}>
    <App />
  </Provider>
), document.getElementById('root'));
