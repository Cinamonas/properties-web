import { observable } from 'mobx';
import AddressModel from './AddressModel';

export default class Property {
  @observable owner = '';
  @observable address = null;
  @observable incomeGenerated = 0;
  @observable coords = null;

  constructor(data) {
    this.owner = data.owner;
    this.address = data.address && new AddressModel(data.address);
    this.incomeGenerated = data.incomeGenerated;
    this.coords = data.coords;
  }
}
