import { observable } from 'mobx';

export default class Address {
  @observable line1 = '';
  @observable line2 = '';
  @observable line3 = '';
  @observable line4 = '';
  @observable postCode = '';
  @observable city = '';
  @observable country = '';

  constructor(data) {
    this.line1 = data.line1 || '';
    this.line2 = data.line2 || '';
    this.line3 = data.line3 || '';
    this.line4 = data.line4 || '';
    this.postCode = data.postCode || '';
    this.city = data.city || '';
    this.country = data.country || '';
  }
}
