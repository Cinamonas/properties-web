import AddressModel from './AddressModel';

describe('AddressModel', () => {

  it('should construct full address', () => {
    const address = {
      line1: '1 Infinite Loop',
      line2: 'foo',
      line3: 'bar',
      line4: 'baz',
      postCode: 'CA 95014',
      city: 'Cupertino',
      country: 'USA',
    };
    expect(new AddressModel(address)).toEqual({
      line1: '1 Infinite Loop',
      line2: 'foo',
      line3: 'bar',
      line4: 'baz',
      postCode: 'CA 95014',
      city: 'Cupertino',
      country: 'USA',
    });
  });

  it('should construct partial address', () => {
    const address = {
      line2: 'foo',
      line3: 'bar',
      line4: 'baz',
    };
    expect(new AddressModel(address)).toEqual({
      line1: '',
      line2: 'foo',
      line3: 'bar',
      line4: 'baz',
      postCode: '',
      city: '',
      country: '',
    });
  });

  it('should ignore unknown properties', () => {
    const address = {
      foo: 'bar',
    };
    expect(new AddressModel(address)).toEqual({
      line1: '',
      line2: '',
      line3: '',
      line4: '',
      postCode: '',
      city: '',
      country: '',
    });
  });

});
