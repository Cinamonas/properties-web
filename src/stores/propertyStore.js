import { observable, action } from 'mobx';
import * as api from '../services/propertiesApi';
import PropertyModel from '../models/PropertyModel';

export class PropertyStore {
  @observable items = [];
  @observable selectedItem = null;
  @observable isLoading = false;

  @action
  load() {
    this.isLoading = true;

    return api.fetchAll()
      .then((items) => {
        this.items.replace(items.map(item => new PropertyModel(item)));
      })
      .finally(() => {
        this.isLoading = false;
      });
  }

  @action
  select(item) {
    this.selectedItem = this.selectedItem !== item ? item : null;
  }
}

export default new PropertyStore();
