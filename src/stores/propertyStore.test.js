jest.mock('../services/propertiesApi');
jest.mock('../models/PropertyModel', () => function PropertyModel({ name }) {
  this.name = name;
});

import propertyStore, { PropertyStore } from './propertyStore';
import * as api from '../services/propertiesApi'
import PropertyModel from '../models/PropertyModel';

describe('propertyStore', () => {

  it('should be a singleton', () => {
    expect(propertyStore instanceof PropertyStore).toBe(true);
  });

  it('should be properly initialized', () => {
    expect(propertyStore.items.peek()).toEqual([]);
    expect(propertyStore.selectedItem).toBe(null);
    expect(propertyStore.isLoading).toBe(false);
  });

  describe('load()', () => {

    it('should replace current properties with newly fetched ones', async () => {
      propertyStore.items.push(...[
        { id: 123 },
        { id: 456 },
      ]);

      api.fetchAll.mockReturnValueOnce(Promise.resolve([{}, {}, {}]));

      await propertyStore.load();

      expect(propertyStore.items.peek().length).toBe(3);
    });

    it('should initialize items as PropertyModel', async () => {
      api.fetchAll.mockReturnValueOnce(Promise.resolve([{ name: 'foo' }]));

      await propertyStore.load();

      expect(propertyStore.items.peek()[0] instanceof PropertyModel).toBe(true);
      expect(propertyStore.items.peek()[0].name).toBe('foo');
    });

    it('should set `isLoading` flag to true', () => {
      api.fetchAll.mockReturnValueOnce(Promise.resolve([]));

      propertyStore.load();

      expect(propertyStore.isLoading).toBe(true);
    });

    it('should set `isLoading` flag back to false when fetch succeeded', async () => {
      api.fetchAll.mockReturnValueOnce(Promise.resolve([]));

      await propertyStore.load();

      expect(propertyStore.isLoading).toBe(false);
    });

    it('should set `isLoading` flag back to false when fetch failed', async () => {
      api.fetchAll.mockReturnValueOnce(Promise.reject());

      try {
        await propertyStore.load();
      } catch (e) {
        expect(propertyStore.isLoading).toBe(false);
      }
    });

  });

  describe('select()', () => {

    it('should set provided item as selected', () => {
      const item = { id: 123 };
      propertyStore.select(item);

      expect(propertyStore.selectedItem).toEqual(item);
    });

    it('should replace currently selected item with the one provided', () => {
      propertyStore.selectedItem = { id: 456 };

      const item = { id: 123 };
      propertyStore.select(item);

      expect(propertyStore.selectedItem).toEqual(item);
    });

    it("should unset selected item if provided item is the one that's currently selected", () => {
      propertyStore.selectedItem = { id: 456 };

      propertyStore.select(propertyStore.selectedItem);

      expect(propertyStore.selectedItem).toBe(null);
    });

  });

});
