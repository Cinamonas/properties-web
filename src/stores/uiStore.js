import { observable, action } from 'mobx';

const MIN_RADIUS = 1000;
const MAX_RADIUS = 30000;

class PropertyStore {
  @observable sidePanelExpanded = false;

  @observable serviceArea = {
    center: {
      lat: 51.5073835,
      lng: -0.1277801,
    },
    radius: 3500,
  };

  @action
  setServiceAreaRadius(radius) {
    this.serviceArea.radius = Math.min(Math.max(radius, MIN_RADIUS), MAX_RADIUS);
  }
}

export default new PropertyStore();
