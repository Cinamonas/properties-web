import React from 'react';
import styles from './Table.scss';

export default function Table({ children }) {
  return (
    <table className={styles.root}>
      {children}
    </table>
  );
}
