import React from 'react';
import { shallow as render } from 'enzyme';

import { App } from './App';
import {
  NavBar,
  PropertyMap,
  SidePanel,
  PropertyList,
} from '../../components';

describe('<App/>', () => {

  describe('<NavBar/> child', () => {

    it('should be rendered', () => {
      const container = render(
        <App
          uiStore={{}}
          propertyStore={{}}
        />
      );

      expect(container.find(NavBar).length).toBe(1);
    });

    it('should be passed correct props', () => {
      const container = render(
        <App
          uiStore={{
            sidePanelExpanded: true,
          }}
          propertyStore={{}}
        />
      );

      expect(container.find(NavBar).props()).toEqual({
        sidePanelExpanded: true,
        onToggleSidePanel: expect.any(Function),
      });
    });

  });

  describe('<PropertyMap/> child', () => {

    it('should be rendered', () => {
      const container = render(
        <App
          uiStore={{}}
          propertyStore={{}}
        />
      );

      expect(container.find(PropertyMap).length).toBe(1);
    });

    it('should be passed correct props', () => {
      const container = render(
        <App
          uiStore={{}}
          propertyStore={{
            items: ['foo', 'bar'],
            selectedItem: 'bar',
          }}
        />
      );

      expect(container.find(PropertyMap).props()).toEqual({
        onLoad: expect.any(Function),
        items: ['foo', 'bar'],
        selectedItem: 'bar',
        onSelect: expect.any(Function),
      });
    });

  });

  describe('<SidePanel/> child', () => {

    it('should be rendered', () => {
      const container = render(
        <App
          uiStore={{}}
          propertyStore={{}}
        />
      );

      expect(container.find(SidePanel).length).toBe(1);
    });

    it('should be passed correct props', () => {
      const container = render(
        <App
          uiStore={{
            sidePanelExpanded: true,
          }}
          propertyStore={{}}
        />
      );

      const props = container.find(SidePanel).props();
      expect(props.expanded).toBe(true);
      expect(typeof props.onToggle).toBe('function');
      expect(props.children.type).toBe(PropertyList);
    });

  });

  describe('<PropertyList/> child', () => {

    it('should be rendered', () => {
      const container = render(
        <App
          uiStore={{}}
          propertyStore={{}}
        />
      );

      expect(container.find(PropertyList).length).toBe(1);
    });

    it('should be passed correct props', () => {
      const container = render(
        <App
          uiStore={{}}
          propertyStore={{
            items: ['foo', 'bar'],
            isLoading: true,
            selectedItem: 'bar',
          }}
        />
      );

      expect(container.find(PropertyList).props()).toEqual({
        items: ['foo', 'bar'],
        isLoading: true,
        selectedItem: 'bar',
        onSelect: expect.any(Function),
      });
    });

  });

});
