import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { autorun } from 'mobx';
import { inject, observer } from 'mobx-react';
import styles from './App.scss';
import {
  NavBar,
  SidePanel,
  PropertyMap,
  PropertyList,
} from '../../components';

export class App extends Component {

  componentDidMount() {
    this.props.propertyStore.load();

    autorun(() => this.offsetMapCenter(this.props.uiStore.sidePanelExpanded));
  }

  toggleSidePanel() {
    const uiStore = this.props.uiStore;

    uiStore.sidePanelExpanded = !uiStore.sidePanelExpanded;
  }

  selectProperty(item, { fromMap = false } = {}) {
    const { uiStore, propertyStore } = this.props;

    propertyStore.select(item);

    if (fromMap && propertyStore.selectedItem === item && !uiStore.sidePanelExpanded) {
      uiStore.sidePanelExpanded = true;
    }
  }

  offsetMapCenter(expanded) {
    if (!this.map) {
      return;
    }

    const sidePanelWidth = findDOMNode(this.sidePanel).offsetWidth;

    const isWideEnough = window.matchMedia(`(min-width: ${Math.ceil(sidePanelWidth * 1.67)}px)`).matches;
    if (!isWideEnough) {
      return;
    }

    let xOffset = findDOMNode(this.sidePanel).offsetWidth / 2;
    if (!expanded) {
      xOffset *= -1;
    }
    setTimeout(() => window.requestAnimationFrame(() => {
      this.map.panBy(xOffset, 0);
    }), 100);
  }

  render() {
    const { uiStore, propertyStore } = this.props;

    return (
      <div className={styles.root}>
        <NavBar
          sidePanelExpanded={uiStore.sidePanelExpanded}
          onToggleSidePanel={() => this.toggleSidePanel()}
        />
        <div className={styles.map}>
          <PropertyMap
            onLoad={map => (this.map = map)}
            items={propertyStore.items}
            selectedItem={propertyStore.selectedItem}
            onSelect={item => this.selectProperty(item, { fromMap: true })}
          />
        </div>
        <SidePanel
          ref={sidePanel => (this.sidePanel = sidePanel)}
          expanded={uiStore.sidePanelExpanded}
          onToggle={() => this.toggleSidePanel()}
        >
          <PropertyList
            items={propertyStore.items}
            isLoading={propertyStore.isLoading}
            selectedItem={propertyStore.selectedItem}
            onSelect={item => this.selectProperty(item)}
          />
        </SidePanel>
      </div>
    );
  }
}


export default inject('uiStore', 'propertyStore')(observer(App));
