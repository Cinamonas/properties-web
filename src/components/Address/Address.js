import React from 'react';
import styles from './Address.scss';

export default function Address({ address }) {
  const fields = Object.values(address);
  const isLast = index => index < fields.length - 1;

  return (
    <address className={styles.root}>
      {fields.map((field, index) => (
        field && [
          field,
          isLast(index) && <br />,
        ]
      ))}
    </address>
  );
}
