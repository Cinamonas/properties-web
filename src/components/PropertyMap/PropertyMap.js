import React from 'react';
import { inject, observer } from 'mobx-react';
import { Map } from '../../components';

const POSITION_STYLES = {
  position: 'absolute',
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
};

function PropertyMap({ uiStore, onLoad, items, selectedItem, onSelect }) {
  const { center, radius } = uiStore.serviceArea;

  return (
    <Map
      onLoad={onLoad}
      containerElement={<div style={POSITION_STYLES} />}
      mapElement={<div style={POSITION_STYLES} />}
      center={center}
      radius={radius}
      markers={items.map(item => ({
        position: item.coords,
        animation: item === selectedItem ? 1 : 0,
        serviceable: isInsideCircle(item.coords, { center, radius }),
        onClick: () => onSelect(item),
      }))}
    />
  );
}

export default inject('uiStore')(observer(PropertyMap));

function isInsideCircle(coords, { center, radius }) {
  const maps = window.google.maps;

  center = new maps.LatLng(center);
  coords = new maps.LatLng(coords);

  return maps.geometry.spherical.computeDistanceBetween(center, coords) <= radius;
}
