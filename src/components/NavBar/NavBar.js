import React, { Component } from 'react';
import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import MdTrackChanges from 'react-icons/lib/md/track-changes';
import MdMenu from 'react-icons/lib/md/menu';
import styles from './NavBar.scss';

export class NavBar extends Component {

  updateSettings() {
    const uiStore = this.props.uiStore;
    const newRadius = Number(window.prompt('Set serviceable area radius:', uiStore.serviceArea.radius)) || 0; // eslint-disable-line no-alert

    if (newRadius) {
      uiStore.setServiceAreaRadius(newRadius);
    }
  }

  render() {
    const { sidePanelExpanded, onToggleSidePanel } = this.props;

    return (
      <div className={styles.root}>
        <h1 className={styles.brand}>Properties</h1>
        <div className={styles.menu}>
          <button className={styles.menuItem} onClick={() => this.updateSettings()}>
            <MdTrackChanges size={22} color="#fff" />
          </button>
          <button
            className={classnames(styles.menuItem, sidePanelExpanded && styles.activeMenuItem)}
            onClick={onToggleSidePanel}
          >
            <MdMenu size={22} color="#fff" />
          </button>
        </div>
      </div>
    );
  }
}

export default inject('uiStore')(observer(NavBar));
