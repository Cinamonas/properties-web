import React from 'react';
import classnames from 'classnames';
import styles from './PropertyList.scss';
import {
  Table,
  Address,
} from '../../components';

const formatMoney = amount => `£${amount.toFixed(2)}`;

export default function PropertyList({ items, isLoading, selectedItem, onSelect }) {
  return (
    <div className={styles.root}>
      <div className={styles.table}>
        <Table>
          <thead>
            <tr>
              <th>Owner</th>
              <th>Address</th>
              <th className={styles.numericCol}>Income Generated</th>
            </tr>
          </thead>
          <tbody>
            {isLoading && (
              <tr>
                <td className={styles.loadingCell} colSpan="3">Loading…</td>
              </tr>
            )}
            {items.map(item => (
              <tr
                key={item.owner}
                className={classnames(styles.row, item === selectedItem && styles.selectedRow)}
                onClick={() => onSelect(item)}
              >
                <td className={styles.ownerCell}>{item.owner}</td>
                <td><Address address={item.address} /></td>
                <td className={styles.numericCol}>{formatMoney(item.incomeGenerated)}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
}
