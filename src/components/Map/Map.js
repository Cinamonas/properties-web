import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, Circle, Marker } from 'react-google-maps';

@withGoogleMap
class Map extends Component {

  maybeCallOnLoad(map) {
    if (map) {
      this.map = map;
    } else if (this.map) {
      this.props.onLoad(this.map);
    }
  }

  render() {
    const { center, radius, markers = [] } = this.props;

    return (
      <GoogleMap
        ref={map => this.maybeCallOnLoad(map)}
        defaultZoom={12}
        defaultCenter={center}
        options={{
          zoomControlOptions: {
            position: window.google.maps.ControlPosition.LEFT_TOP,
          },
          streetViewControl: false,
        }}
        onProjectionChanged={() => this.maybeCallOnLoad()}
      >
        <Circle
          center={center}
          radius={radius}
          options={{
            clickable: false,
            fillColor: '#57c4c6',
            fillOpacity: 0.25,
            strokeColor: '#57c4c6',
            strokeOpacity: 0.75,
            strokeWeight: 2,
          }}
        />
        {markers.map(({ ...marker, serviceable }) => (
          <Marker
            key={`${marker.position.lat},${marker.position.lng}`}
            {...marker}
            opacity={serviceable ? 1 : 0.5}
          />
        ))}
      </GoogleMap>
    );
  }
}

export default Map;
