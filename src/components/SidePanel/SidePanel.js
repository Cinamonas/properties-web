import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './SidePanel.scss';

class SidePanel extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { expanded, onToggle, children } = this.props;

    return (
      <div className={classnames(styles.root, expanded && styles.expanded)}>
        <div className={styles.contentWrapper}>
          {children}
        </div>
        <button className={styles.toggle} onClick={onToggle} />
      </div>
    );
  }
}

export default SidePanel;
