# Properties Web App

## Prerequisites

- [Node.js](https://nodejs.org/en/) 6 or higher (for proper ES2015 support). Can be easily installed with [nvm](https://github.com/creationix/nvm): `nvm install 6.10`
- (Optional) [Yarn](https://yarnpkg.com/) instead of [npm](https://www.npmjs.com/) for blazing fast and reliable dependency management

## Installation

1. `yarn` (or `npm install`)

## Commands

- `yarn build` (or `npm run build`) builds app
- `yarn start` (or `npm run build`) starts app (port can be optionally overridden by providing `PORT` env variable)
- `yarn dev` (or `npm run dev`) starts [`webpack-dev-server`](https://webpack.js.org/configuration/dev-server/) (with auto-reloading on changes)
- `yarn test` (or `npm run test`) runs test suite
- `yarn lint` (or `npm run lint`) runs linter
- See `scripts` in `package.json` for more
