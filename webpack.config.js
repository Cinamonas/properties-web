const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';
const IS_PROD = NODE_ENV === 'production';
const SOURCE_DIR = path.resolve(__dirname, 'src');
const DESTINATION_DIR = path.resolve(__dirname, 'dist');

module.exports = {
  context: SOURCE_DIR,
  entry: {
    app: [
      'core-js',
      './index.js',
    ],
  },
  output: {
    filename: IS_PROD ? '[name].[hash].js' : '[name].bundle.js',
    path: DESTINATION_DIR,
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: SOURCE_DIR,
        use: 'babel-loader',
      },
      {
        test: /\.scss$/,
        include: SOURCE_DIR,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[name]-[local]__[hash:base64:5]',
                importLoaders: 1,
                minimize: IS_PROD,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
            'postcss-loader',
          ],
        }),
      },
    ],
  },
  plugins: (() => {
    const plugins = [];

    plugins.push(...[
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(NODE_ENV),
        },
      }),
      new HtmlPlugin({
        template: 'index.tpl.html',
      }),
      new ExtractTextPlugin({
        filename: IS_PROD ? '[name].[contenthash].css' : '[name].bundle.css',
        allChunks: true,
      }),
    ]);

    if (IS_PROD) {
      plugins.push(...[
        new webpack.optimize.UglifyJsPlugin(),
      ]);
    }

    return plugins;
  })(),
  devtool: IS_PROD ? 'cheap-module-source-map' : 'cheap-module-eval-source-map',
  devServer: {
    contentBase: SOURCE_DIR,
  },
};
